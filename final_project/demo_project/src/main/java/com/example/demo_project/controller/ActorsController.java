package com.example.demo_project.controller;

import com.example.demo_project.models.Actor;
import com.example.demo_project.models.Film;
import com.example.demo_project.services.ActorsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ActorsController {

    private final ActorsService actorsService;

    @GetMapping("/actors/{actor-id}")
    public String getActorPage( Model model, @PathVariable("actor-id") Integer actorId) {
        Actor actor = actorsService.getActor(actorId);
        List<Film> films = actor.getFilms();
        if(films == null)
            films = new ArrayList<>();

        model.addAttribute("actor", actor);
        model.addAttribute("films", films);
        return "actor";
    }
}
