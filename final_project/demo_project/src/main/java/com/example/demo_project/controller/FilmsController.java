package com.example.demo_project.controller;


import com.example.demo_project.forms.ActorAddForm;
import com.example.demo_project.forms.FilmForm;
import com.example.demo_project.models.Actor;
import com.example.demo_project.models.Film;
import com.example.demo_project.services.ActorsService;
import com.example.demo_project.services.FilmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FilmsController {
    private final FilmsService filmsService;
    private final ActorsService actorsService;
    private Principal principal;
    private FilmForm form;

    @Autowired
    public FilmsController(FilmsService filmsService, ActorsService actorsService) {

        this.filmsService = filmsService;
        this.actorsService = actorsService;
    }


    @GetMapping("/films/search")
    public String getFilmsSearchPage()
    {
        return "search";
    }

    @GetMapping("/films/add")
    public String getAddFilmPage()
    {
        return "addFilm";
    }

    @GetMapping("/films/all")
    public String getAll(Model model)
    {

        List<Film> films = filmsService.getAllFilms();

        model.addAttribute("films", films);
        return "search_result";
    }



    @PostMapping("/films/addActor")
    public String addActor(Principal principal, @Valid ActorAddForm form) {

        Actor actor = actorsService.getActorByName(form.getName());
        if(actor == null) {
            if(form.getAge() == null)
                return "redirect:/films/" + form.getFilmId();

            actor = actorsService.createActor(form.getName(), form.getAge());
        }
        filmsService.addActor(Integer.valueOf(form.getFilmId()), actor);

        return "redirect:/films/" + form.getFilmId();
    }


    @GetMapping("/films/{film-id}")
    public String getFilmPage(Model model, @PathVariable("film-id") Integer filmId) {
        Film film = filmsService.getFilm(filmId);
        List<Actor> actors = film.getActors();
        if(actors == null)
            actors = new ArrayList<Actor>();

        model.addAttribute("film", film);
        model.addAttribute("actors", actors);
        return "film";
    }

    @GetMapping(value = "/films", params = {"title"})
    public  String getFilmsByTitle(Model model, @RequestParam("title") String title)
    {
        List<Film> films = filmsService.getFilmsByTitle(title);

        model.addAttribute("films", films);
        return "search_result";
    }


    @GetMapping("/films")
    public String getFilmsByActorName(Model model, @RequestParam("actorName") String actorName)
    {
        Actor actor = actorsService.getActorByName(actorName);
        List<Film> films;
        if(actor == null)
            films = new ArrayList<>();
        else
            films = actor.getFilms();

        model.addAttribute("films", films);
        return "search_result";
    }
}
