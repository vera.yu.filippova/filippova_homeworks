package com.example.demo_project.forms;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ActorAddForm {
    private String filmId;

    @NotEmpty
    private String name;

    private Integer age;
}
