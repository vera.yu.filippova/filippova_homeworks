package com.example.demo_project.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class FilmForm {
    @NotEmpty
    @Length(max = 100)
    private String title;

    @NotNull
    private Integer releasedYear;

    @NotEmpty
    @Length(max = 1000)
    private String description;

    @NotNull
    private double rating;

    private String actors;
}
