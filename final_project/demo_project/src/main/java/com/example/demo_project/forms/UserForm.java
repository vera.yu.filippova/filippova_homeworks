package com.example.demo_project.forms;

import lombok.Data;

@Data
public class UserForm {

    private String login;

    private String password;

}
