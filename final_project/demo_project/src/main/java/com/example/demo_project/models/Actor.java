package com.example.demo_project.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "actors")
public class Actor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty
    private String name;

    private Integer age;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "films_actors_link",
            joinColumns = {
                    @JoinColumn(name = "actor_id",
                            referencedColumnName = "id",
                            nullable = false,
                            updatable = false)} ,
            inverseJoinColumns = {
                    @JoinColumn(name = "film_id",
                            referencedColumnName = "id",
                            nullable = false,
                            updatable = false)}
    )
    private List<Film> films;
}
