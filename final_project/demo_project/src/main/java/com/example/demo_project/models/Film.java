package com.example.demo_project.models;

import lombok.*;
import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "films")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;

    private String title;

    private Integer releasedyear;

    private String description;

    private double rating;

    @ManyToOne()
    @JoinColumn(name = "record_author")
    private User recordAuthor;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "films_actors_link",
            joinColumns = {
                @JoinColumn(name = "film_id", referencedColumnName = "id",
                    nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "actor_id", referencedColumnName = "id",
                            nullable = false, updatable = false)}
        )
    private List<Actor> actors;

    public void addActor(Actor actor) {
        if(!actors.contains(actor))
            actors.add(actor);
    }
}