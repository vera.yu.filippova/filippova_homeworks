package com.example.demo_project.models;

import lombok.*;

import javax.management.relation.Role;
import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;



    @OneToMany(mappedBy = "recordAuthor")
    private List<Film> films;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}
