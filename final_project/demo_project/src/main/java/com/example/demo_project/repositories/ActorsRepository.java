package com.example.demo_project.repositories;


import com.example.demo_project.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorsRepository extends JpaRepository<Actor, Integer> {

    Actor getActorById(int id);

    Actor getActorByName(String actorName);
}
