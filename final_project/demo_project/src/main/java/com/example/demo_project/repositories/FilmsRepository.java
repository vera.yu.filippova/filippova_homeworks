package com.example.demo_project.repositories;


import com.example.demo_project.models.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface FilmsRepository extends JpaRepository<Film, Integer> {

    Film getFilmById(int id);

    List<Film> findAllByTitle(String title);

    List<Film> findAll();
}

