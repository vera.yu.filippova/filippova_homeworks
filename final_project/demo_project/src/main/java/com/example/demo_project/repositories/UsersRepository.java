package com.example.demo_project.repositories;

import com.example.demo_project.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Integer> {
    Optional<User> findByLogin(String login);
}

