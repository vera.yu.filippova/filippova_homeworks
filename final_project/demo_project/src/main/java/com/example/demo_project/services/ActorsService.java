package com.example.demo_project.services;

import com.example.demo_project.models.Actor;


public interface ActorsService {

    Actor getActor(Integer actorId);

    Actor getActorByName(String actorName);

    Actor createActor(String name, Integer age);
}
