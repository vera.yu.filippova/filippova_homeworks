package com.example.demo_project.services;

import com.example.demo_project.models.Actor;
import com.example.demo_project.repositories.ActorsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@RequiredArgsConstructor
@Component
public class ActorsServiceImpl implements ActorsService {

    private final ActorsRepository actorsRepository;

    @Override
    public Actor getActor(Integer actorId) {
        return actorsRepository.getActorById(actorId);
    }

    @Override
    public Actor getActorByName(String actorName) {
        return actorsRepository.getActorByName(actorName);
    }

    @Override
    public Actor createActor(String name, Integer age) {
        return actorsRepository.save(
                Actor.builder()
                        .age(age)
                        .name(name)
                        .build());
    }
}
