package com.example.demo_project.services;

import com.example.demo_project.forms.FilmForm;
import com.example.demo_project.models.Actor;
import com.example.demo_project.models.Film;
import com.example.demo_project.models.User;
import java.util.List;

public interface FilmsService {

    Film getFilm(Integer filmId);

    List<Film> getFilmsByTitle(String name);

    void addFilm(FilmForm form, User author);

    List <Film> getAllFilms();

    void addActor(Integer filmId, Actor actor);
}
