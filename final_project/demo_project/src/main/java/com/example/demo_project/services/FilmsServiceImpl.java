package com.example.demo_project.services;

import com.example.demo_project.forms.FilmForm;
import com.example.demo_project.models.Actor;
import com.example.demo_project.models.Film;
import com.example.demo_project.models.User;
import com.example.demo_project.repositories.FilmsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class FilmsServiceImpl implements FilmsService {

    private final FilmsRepository filmsRepository;

    @Override
    public Film getFilm(Integer filmId) {
        return filmsRepository.getFilmById(filmId);
    }

    @Override
    public void addFilm(FilmForm form, User author) {

        Film film = Film.builder()
                .title(form.getTitle())
                .description(form.getDescription())
                .releasedyear(form.getReleasedYear())
                .recordAuthor(author)
                .rating(form.getRating())
                .build();

        filmsRepository.save(film);
    }

    @Override
    public List<Film> getAllFilms() {
        return  filmsRepository.findAll();
    }

    @Override
    public void addActor(Integer filmId, Actor actor) {
        Film film = filmsRepository.getFilmById(filmId.intValue());
        film.addActor(actor);
        filmsRepository.save(film);
    }

    @Override
    public List<Film> getFilmsByTitle(String title) {
        return  filmsRepository.findAllByTitle(title);
    }
}
